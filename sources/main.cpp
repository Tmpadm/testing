/*!
@file
@date 04.10.2023
@version 1.0.0
@author Tmp Adm
@brief The file with realization of the main function of the application.
@copyright The software is distributed by the
    [zlib/libpng](https://opensource.org/licenses/zlib-license.php "The zlib/libpng license")
    license.
> Copyright &copy; 2024 Tmp Adm <tmpadm@rambler.ru>
>
> This software is provided 'as-is', without any express or implied
> warranty. In no event will the authors be held liable for any damages
> arising from the use of this software.
>
> Permission is granted to anyone to use this software for any purpose,
> including commercial applications, and to alter it and redistribute it
> freely, subject to the following restrictions:
>
> 1. The origin of this software must not be misrepresented; you must not
>    claim that you wrote the original software. If you use this software
>    in a product, an acknowledgment in the product documentation would
>    be appreciated but is not required.
>
> 2. Altered source versions must be plainly marked as such, and must not
>    be misrepresented as being the original software.
>
> 3. This notice may not be removed or altered from any source
>    distribution
@details Contains the realization of the main function of the application.
    The function just print a simple text string.
*/
/**
@brief The main function of the application.
@param argc The quantity of arguments in command line;
@param argv The array of strings with arguments of command line;
@return the result of execution of the application.
*/
#include <iostream>
int main(int argc, char** argv)
{
	std::cout << "TESTING..." << std::endl << "DONE!!!" << std::endl;
	return 0;
}
