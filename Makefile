
ifeq ($(basename $(notdir $(MAKE))), mingw32-make)
  CMAKE_GENERATOR = "MinGW Makefiles"
  MAKE_DIR = md
  REMOVE_DIR = rd /s /q
else
  CMAKE_GENERATOR = "Unix Makefiles"
  MAKE_DIR = mkdir -p
  REMOVE_DIR = rm -rf
endif

CPU_ARCH ?= x86
PREFIX ?= /usr

all: release
test: release-test

release:
	@-${MAKE_DIR} bin
	@-${MAKE_DIR} build
	@cd build && cmake -G $(CMAKE_GENERATOR) -D CMAKE_BUILD_TYPE=Release -D CMAKE_INSTALL_PREFIX=$(PREFIX) -U APPLICATION_PORTABLE ..
	@$(MAKE) -C build

portable-release:
	@-${MAKE_DIR} bin
	@-${MAKE_DIR} build
	@cd build && cmake -G $(CMAKE_GENERATOR) -D CMAKE_BUILD_TYPE=Release -D CMAKE_INSTALL_PREFIX=$(PREFIX) -D APPLICATION_PORTABLE=ON ..
	@$(MAKE) -C build

release-test: release
	@$(MAKE) -C build test

debug:
	@-${MAKE_DIR} bin
	@-${MAKE_DIR} build
	@cd build && cmake -G $(CMAKE_GENERATOR) -D CMAKE_BUILD_TYPE=Debug -D CMAKE_INSTALL_PREFIX=$(PREFIX) -U APPLICATION_PORTABLE ..
	@$(MAKE) -C build

portable-debug:
	@-${MAKE_DIR} bin
	@-${MAKE_DIR} build
	@cd build && cmake -G $(CMAKE_GENERATOR) -D CMAKE_BUILD_TYPE=Debug -D CMAKE_INSTALL_PREFIX=$(PREFIX) -D APPLICATION_PORTABLE=ON ..
	@$(MAKE) -C build

debug-test: debug
	@$(MAKE) -C build test

install: release
	@echo "Performing installation"
	@$(MAKE) -C build install

clean:
	@echo "Cleaning build directory"
	-@$(MAKE) -C build clean

distclean: clean
	@echo "Removing build directory"
	-@${REMOVE_DIR} bin
	-@${REMOVE_DIR} build
